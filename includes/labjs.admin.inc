<?php
/**
 * @file
 *   LABjs module admin settings
 */

/**
 * Form for configuring the module.
 */
function labjs_admin_settings_form() {
  $form = array();

  $form['labjs_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable the LABjs library.'),
    '#default_value' => variable_get('labjs_enabled', LABJS_ENABLED),
  );
  $form['labjs_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debug mode for the LABjs library.'),
    '#default_value' => variable_get('labjs_debug', LABJS_DEBUG),
  );
  $form['pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page specific LABjs settings'),
    '#collapsible' => TRUE,
  );
  $form['pages']['labjs_pages_choice'] = array(
    '#type' => 'radios',
    '#title' => t('Enable LABjs on specific pages'),
    '#options' => array(t('Enable on every page except the listed pages.'), t('Enable on only the listed pages.')),
    '#default_value' => variable_get('labjs_pages_choice', 0),
  );
  $form['pages']['labjs_pages_list'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => variable_get('labjs_pages_list', ''),
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
  );

  $form['pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page specific LABjs settings'),
  );
  // Taken from block_admin_configure().
  $access = user_access('use PHP for settings');
  $visibility = variable_get('labjs_visibility', BLOCK_VISIBILITY_NOTLISTED);
  $pages = variable_get('labjs_pages', '');
  if ($visibility == BLOCK_VISIBILITY_PHP && !$access) {
    $form['pages']['visibility'] = array(
      '#type' => 'value',
      '#value' => $visibility,
    );
    $form['pages']['pages'] = array(
      '#type' => 'value',
      '#value' => $pages,
    );
  }
  else {
    $options = array(
      BLOCK_VISIBILITY_NOTLISTED => t('All pages except those listed'),
      BLOCK_VISIBILITY_LISTED => t('Only the listed pages'),
    );
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

    if (module_exists('php') && $access) {
      $options += array(BLOCK_VISIBILITY_PHP => t('Pages on which this PHP code returns <code>TRUE</code> (experts only)'));
      $title = t('Pages or PHP code');
      $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    else {
      $title = t('Pages');
    }
    $form['pages']['labjs_visibility'] = array(
      '#type' => 'radios',
      '#title' => t('Enable LABjs on specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['pages']['labjs_pages'] = array(
      '#type' => 'textarea',
      '#title' => '<span class="element-invisible">' . $title . '</span>',
      '#default_value' => $pages,
      '#description' => $description,
    );
  }

  return system_settings_form($form);
}